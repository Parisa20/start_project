import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: HomePage(),
  ));

}

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    void click() {}

    var width = MediaQuery
        .of(context)
        .size
        .width;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: false,
        backgroundColor: Color(0xffffff).withOpacity(1.0),
        title: Text('Bookify',
            style: TextStyle(color: Colors.black, fontFamily: 'RussoOne')),
        actions: [
          Builder(
            builder: (context) =>
                IconButton(
                  icon: Icon(
                    Icons.menu,
                    color: Colors.black,
                  ),
                  onPressed: () => Scaffold.of(context).openEndDrawer(),
                  tooltip: MaterialLocalizations
                      .of(context)
                      .openAppDrawerTooltip,
                ),
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text('BESTSELLER',
                    style: TextStyle(
                        color: Color(0X925F11).withOpacity(1.0),
                        fontFamily: 'RussoOne')),
                Container(
                    child: SizedBox(
                      width: width / 2,
                    )
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        onPressed: click,
                        child: Text('See All', style: TextStyle(color: Color(
                            0X925F11).withOpacity(1.0),
                            fontFamily: 'RussoOne'),),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 430.0,
            height: 230.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                FlatButton(
                  onPressed: click,
                  child: Image.asset('assets/images/image1.jpg'),
                ),
                FlatButton(
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return SecondPage();
                    }));
                  },
                  child: Image.asset('assets/images/image4.jpg'),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text('NEW RELEASE',
                    style: TextStyle(
                        color: Color(0X925F11).withOpacity(1.0),
                        fontFamily: 'RussoOne')),
                Container(
                    child: SizedBox(
                      width: width / 2,
                    )
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        onPressed: click,
                        child: Text('See All', style: TextStyle(color: Color(
                            0X925F11).withOpacity(1.0),
                            fontFamily: 'RussoOne'),),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 430.0,
            height: 230.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      FlatButton(
                        onPressed: click,
                        child: Row(
                          children: <Widget>[
                            Image.asset('assets/images/image2.jpg'),
                          ],
                        ),
                      ),
                    ],
                  ),),
                FlatButton(
                  onPressed: click,
                  child: Image.asset('assets/images/image3.jpg'),
                ),
              ],
            ),
          ),
          Container(
            width: 430.0,
            padding: EdgeInsets.all(20.0),
            child: Row(
              children: <Widget>[
                Container(
                    child: Column(
                      children: <Widget>[
                        Text('A Summer Break',
                            style: TextStyle(color: Colors.black)),
                        Text('Hatchet Livre Publications',
                            style: TextStyle(color: Color(0x8c8d8f).withOpacity(
                                1.0)))
                      ],
                    )
                ),
                Container(
                  child: SizedBox(
                    width: 32.0,
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Text('Just Another Day',
                          style: TextStyle(color: Colors.black)),
                      Text('Wiley Publications', style: TextStyle(color: Color(
                          0x8c8d8f).withOpacity(1.0)))
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.library_books, color: Colors.black),
            title: Text('Library',
                style: TextStyle(color: Colors.black, fontFamily: 'RussoOne'))),
        BottomNavigationBarItem(
            icon: Icon(Icons.book, color: Colors.black),
            title: Text('Books',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'RussoOne',
                ))),
        BottomNavigationBarItem(
            icon: Icon(Icons.search, color: Colors.black),
            title: Text('Search',
                style: TextStyle(color: Colors.black, fontFamily: 'RussoOne'))),
        BottomNavigationBarItem(
            icon: Icon(Icons.add_comment, color: Colors.black),
            title: Text('Comments',
                style: TextStyle(color: Colors.black, fontFamily: 'RussoOne'))),
        BottomNavigationBarItem(
            icon: Icon(Icons.help, color: Colors.black),
            title: Text('Help',
                style: TextStyle(color: Colors.black, fontFamily: 'RussoOne')))
      ]),
      endDrawer: SizedBox(
        width: width * 3 / 4,
        child: Drawer(
        ),
      ),
    );
  }
  }

  class SecondPage extends StatefulWidget {

    @override
    _SecondPageState createState() => _SecondPageState();
  }

  class _SecondPageState extends State<SecondPage> {

    @override
    Widget build(BuildContext context) {
      void click() {}

      var width = MediaQuery
          .of(context)
          .size
          .width;

      item(){
        return Container(
          child:Row(
            mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: width/2,
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(10.0),
                            child: Text('The Lord Of Rings', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26.0)),
                          ),
                          Container(
                            child: ListTile(
                              title: Text('J.R.R. Tolkien', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0)),
                              subtitle: Text('3 Volumes', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: Color(0x8c8d8f).withOpacity(1.0))) ,
                              leading: CircleAvatar(
                                radius: 23.0,
                                backgroundImage: AssetImage('assets/images/image5.jpg',)
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                            child: Image.asset('assets/images/image4.jpg',scale: 2.5,),
                          )
            ]
          ),
        );
      }

      return Scaffold(
        appBar: AppBar(
          centerTitle: false,
          elevation: 0,
         actions: <Widget>[
             Container(
               child:
               IconButton(icon: Icon(Icons.save_alt), color: Color(0x8c8d8f).withOpacity(1.0), onPressed: click),

             ),
           Container(
             child:
             IconButton(icon: Icon(Icons.favorite_border), color: Color(0x8c8d8f).withOpacity(1.0), onPressed: click),
           )
              ],
          backgroundColor: Color(0xffffff).withOpacity(1.0),
          iconTheme: IconThemeData(color: Color(0X925F11).withOpacity(1.0)),
        ),
        body: ListView(
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            Container(
              child: item(),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  FlatButton(
                    onPressed: click,
                    child: Text('Fantasy', style: TextStyle(color: Color(
                        0X925F11).withOpacity(1.0),
                        fontFamily: 'RussoOne',backgroundColor: Color(0xdecdb5).withOpacity(1.0)),),
                  ),
                  FlatButton(
                    onPressed: click,
                    child: Text('Adventure', style: TextStyle(color: Color(
                        0X925F11).withOpacity(1.0),
                        fontFamily: 'RussoOne',backgroundColor: Color(0xdecdb5).withOpacity(1.0) ),
                    ),
                  ),
                  FlatButton(
                    onPressed: click,
                    child: Text('Series', style: TextStyle(color: Color(
                        0X925F11).withOpacity(1.0),
                        fontFamily: 'RussoOne',backgroundColor: Color(0xdecdb5).withOpacity(1.0)),),
                  ),
                  FlatButton(
                    onPressed: click,
                    child: Text('Fiction', style: TextStyle(color: Color(
                        0X925F11).withOpacity(1.0),
                        fontFamily: 'RussoOne',backgroundColor: Color(0xdecdb5).withOpacity(1.0)),),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
  }
